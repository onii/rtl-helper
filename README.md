# RTL Helper

A script to make accessing rtl_fm simpler.

Similar to [internet-radio](https://gitlab.com/onii/internet-radio),
but designed for FM radio.

---

Requires `rtl_fm`, 

`libsox-fmt-mp3` + `mpv` to play, 

and `ffmpeg` for recording.

---

## Examples:

*Play 90.9MHz*:

    ./rtl-helper.py 90.9

*Play 90.9MHz with 30 minute sleep timer*:

    # rtl-helper.py <Freq in MHz> <sleep timer in minutes> 
    ./rtl-helper.py 90.9 30

*Record with cron*:

    # rtl-helper.py <Freq in MHz> <duration in minutes> <record flag> <Rename keyword (optional)>
    #
    # Revival
    0 10 * * 0 python3 ~/rtl-helper/rtl-helper.py 90.9 120 rec Revival >/dev/null 2>&1
    # Snap Judgement
    0 12 * * 6 python3 ~/rtl-helper/rtl-helper.py 89.3 60 rec >/dev/null 2>&1
