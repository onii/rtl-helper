#!/usr/bin/python3

import time, subprocess, sys

def record(station, rectimer):
    filename = (time.strftime("%m-%d_%H_") + station + '.mp3')
    if len(sys.argv) == 5:
        filename = (time.strftime("%m-%d_%H_") + sys.argv[4] + '.mp3')
    try:
        print('Filename:', filename)
        addtime = (time.strftime("%I:%M %p", time.localtime(time.time() + 
        rectimer)))
        print('Recording for:', (rectimer/60),'minutes.', '(' + addtime + ')')
        rtl = subprocess.Popen(['rtl_fm','-f',station,'-M','fm','-s','200k',
        '-E','deemp','-r','44100'], stdout=subprocess.PIPE)
        ffmpeg = subprocess.call(['ffmpeg','-f','s16le','-ac','1','-ar',
        '44100','-i','pipe:0','-acodec','libmp3lame','-ab','96k',filename],
        stdin=rtl.stdout, timeout=rectimer)
    except KeyboardInterrupt:
        time.sleep(1)
        return
    except subprocess.TimeoutExpired:
        return

def play(station, sleeptimer):
    try:
        rtl = subprocess.Popen(['rtl_fm','-f',station,'-M','fm','-s','200k',
        '-E','deemp'], stdout=subprocess.PIPE)
        if sleeptimer:
            sox = subprocess.Popen(['sox','-traw','-r200k','-es','-b16','-c1',
            '-V1','-','-tmp3','-'], stdin=rtl.stdout, stdout=subprocess.PIPE)
            mpv = subprocess.run(['mpv','-'], stdin=sox.stdout,
            timeout=sleeptimer)
        else:
            sox = subprocess.Popen(['sox','-traw','-r200k','-es','-b16','-c1',
            '-V1','-','-tmp3','-'], stdin=rtl.stdout, stdout=subprocess.PIPE)
            mpv = subprocess.run(['mpv','-'], stdin=sox.stdout)
    except KeyboardInterrupt:
        time.sleep(1)
        return
    except subprocess.TimeoutExpired:
        return

def info():
    print("Usage:\n" + './rtl-helper.py <freq in MHz> <sleeptimer in M>')

if len(sys.argv) == 1:
    input = input('Input freq in MHz: ')
    splitinput = input.split(" ")
    if len(splitinput) == 2:
        play(splitinput[0] + 'M', (int(splitinput[1])*60))
    else:
        play(input + 'M', None)
if len(sys.argv) == 2:
    play(sys.argv[1] + 'M', None)
if len(sys.argv) == 3:
    play(sys.argv[1] + 'M', (int(sys.argv[2])*60))
if len(sys.argv) == 4 or len(sys.argv) == 5:
    record(sys.argv[1] + 'M', (int(sys.argv[2])*60))
if len(sys.argv) > 5:
    info()
